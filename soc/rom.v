`define ROM_SIZE 512 //bytes

module rom(input [0:31] addr,input clk,output reg [0:7] data_out);
    reg [0:7] rommem[0:`ROM_SIZE-1];

    initial begin
        //load rom data from file,only used to debug
    end

    always @(*)
    begin
        data_out=rommem[addr];
    end 
endmodule
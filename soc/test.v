`include "soc/clock.v"
`include "soc/core.v"

module test;

wire clk;
reg cpu_enable;
reg ext_int; 

core cpu (.clk (clk),.cpu_enable (cpu_enable),.ext_int (ext_int));

initial begin 
  #(20)
  cpu_enable = 'b1;
  #(600)
  cpu_enable = 'b0;
end

initial begin
    $dumpfile("test.vcd");
    $dumpvars(0,test);
end

endmodule 
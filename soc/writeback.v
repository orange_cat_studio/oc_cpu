`include "soc/register_group.v"
`include "soc/clock.v"
`include "soc/encode.v"

module writeback(input clk,input code);

always @(posedge clk)
begin
    case (code):
        `OP_LOAD:
        begin
         core.reg_idx<=en.reg1;
         core.reg_op<=`REG_WRITE;
         core.reg_in<=core.alu_out;
        end
        `OP_ADD:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op<=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_DEC:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op<=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end 
        `OP_MUL:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_INC:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_DEC:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_AND:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_OR:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OP_XOR:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OR_NOT:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OR_SHL:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op<=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
        `OR_SHR:
        begin
        core.reg_idx<=en.reg1;
        core.reg_op<=`REG_WRITE;
        core.reg_in<=core.alu_out;
        end
    endcase

end
endmodule
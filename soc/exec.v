`include "header/register_group.vh"
`include "header/alu.vh"
`include "header/opcode.vh"

module exec(input clk,input code,output reg out_reg1,out_reg2);

always @(posedge clk)
    case (code):
        `OP_LOAD:
            begin
            core.alu_op1<=en.reg2;
            core.alu_op2<=0;
            core.alu_op<=`ALU_ADD;
            end
        `OP_ADD:
            begin
            core.alu_op1<=en.reg1;
            core.alu_op2<=en.reg2;
            core.alu_op<=`ALU_ADD;
            end
        `OP_DEC:
        begin
            core.alu_op1<=en.reg1;
            core.alu_op2<=en.reg2;
            core.alu_op<=`ALU_DEC;
        end
        `OP_MUL:
        begin
            core.alu_op1<=en.reg1;
            core.alu_op2<=en.reg2;
            core.alu_op<=`ALU_MUL;
        end

        `OP_DIV:
            begin
            core.alu_op1<=en.reg1;
            core.alu_op2<=en.reg2;
            core.alu_op<=`ALU_DIV;
            end
        `OP_AND:
            begin
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_AND;
            end
        `OP_OR:
            begin 
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_OR;
            end
        `OR_XOR:
            begin 
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_XOR;
            end
        `OR_NOT:
            begin 
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_NOT;
            end
        `OR_SHL:
            begin
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_SHL;
            end
        `OR_SHR:
            begin 
                core.alu_op1<=en.reg1;
                core.alu_op2<=en.reg2;
                core.alu_op<=`ALU_SHR;
            end 
    endcase

endmodule

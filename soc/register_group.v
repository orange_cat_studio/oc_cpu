`include "header/register_group.vh"

module register_group (
   input [0:7] idx, //register idx
   input [0:31] in, //data in 
   input op, //read/write
   input clk, //clock
   input rst, //register reset
   output reg [0:31] out //data out
);

reg [0:31] r[0:`REG_MAX];
reg [0:3] i=0;
reg [0:30] data_out;

//register operator always work in clk posedge 
always @(*) begin
    //rst signal always low level enable
    if(!rst)
        begin
            out='h0; //init out to 0
            while (i<`REG_MAX) begin
                i++;
                r[i]=0;
            end 
        end

    if(op==`REG_READ) begin
        case (idx)
            `REG_AR: data_out<=r[`REG_AR];
            `REG_BR: data_out<=r[`REG_BR];
            `REG_CR: data_out<=r[`REG_CR];
            `REG_DR: data_out<=r[`REG_DR];
            `REG_CR0: data_out<=r[`REG_CR0];
            `REG_CR1: data_out<=r[`REG_CR1];
            `REG_CR2: data_out<=r[`REG_CR2];
            `REG_CR3: data_out<=r[`REG_CR3];
            `REG_CR4: data_out<=r[`REG_CR4];
            default: data_out<=data_out;
        endcase
        //output value
        out <= data_out;
        end
    else if(op==`REG_WRITE) begin
        case (idx)
            `REG_AR : r[`REG_AR]<=in;
            `REG_BR : r[`REG_BR]<=in;
            `REG_CR : r[`REG_CR]<=in;
            `REG_DR : r[`REG_DR]<=in;
            `REG_CR0: r[`REG_CR0]<=in;
            `REG_CR1: r[`REG_CR1]<=in;
            `REG_CR2: r[`REG_CR2]<=in;
            `REG_CR3: r[`REG_CR3]<=in;
            `REG_CR4: r[`REG_CR4]<=in;
            default:  //TODO: exception
             ;
        endcase
        end
    end

endmodule

`include "header/clock.vh"
`include "header/ram.vh"

module ram(input rw,input [0:31] addr,input clk,input [0:7] data_in,output reg [0:7] data_out);

reg [0:7] rammem[0:`RAM_SIZE-1]; //memory space

always @(*)
    begin 
        if(rw==`RAM_READ)
            begin
                //read a byte
                data_out <= rammem[addr];        
            end
        else if(rw==`RAM_WRITE)
            begin
                //delay to clock nesedge
                #(`CLK_CYCLE/2);
                //write a byte
                rammem[addr] <= data_in;
            end
    end 
endmodule
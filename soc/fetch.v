`include "header/register_group.vh"
`include "header/clock.vh"

`define INS_LEN 5 //instruct length

`define FETCH_DELAY (`CLK_CYCLE/`INS_LEN)+2

module fetch(input clk);

//fetch instruction
always @(posedge clk)
begin
          if(core.init_done)
          begin  
               //get instruction
               core.reg_idx <= `REG_IP;
               core.reg_op <= `REG_READ;
               #(`FETCH_DELAY)
               
               //read instruction data from ROM
               for(reg i=0;i<`INS_LEN;i++)
               begin 
                    core.rom_addr<=core.reg_out;
                    #(`FETCH_DELAY);
                    core.ic_buff[i]<=core.romdata_out;
               end
               #(`FETCH_DELAY);
          end
end

endmodule
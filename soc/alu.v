`include "header/alu.vh"

module alu (input [0:31] data1,input [0:31] data2,input [0:3] op,input clk,output reg [0:31] res1,output reg [0:31] res2);
    always @(posedge clk)
        begin
            case (op)
                `ALU_ADD:
                    res1 <= data1+data2;
                `ALU_SUB:
                    res1 <= data1-data2;
                `ALU_DIV:
                    res1 <= data1*data2;
                `ALU_MUL: 
                    res1 <= data1/data2;
                `ALU_AND:
                    res1 <= data1&data2;
                `ALU_OR:
                    res1 <= data1|data2;
                `ALU_XOR:
                    res1 <= data1^data2;
                `ALU_NOT:
                    res1 <= ~data1;
                `ALU_SHL:
                    res1 <= data1<<data2;
                `ALU_SHR:
                    res1 <= data1>>data2;
                `ALU_INC:
                    res1 <= data1++;
                `ALU_DEC:
                    res1 <= data1--;
                default: 
                    res1 <= res1;
            endcase
        end 
endmodule
`include "header/alu.vh"
`include "header/opcode.vh"
`include "header/register_group.vh"
`include "header/clock.vh"

module encode(input clk, input [0:31] data ,output reg [0:7] op);
reg [0:7] code;
reg [0:31] mem;
reg [0:7] reg1;
reg [0:7] reg2;
reg [0:7] num;
reg [0:31] n_type;

always @(posedge clk)
begin 

//opnum type unknown
if(data>>16 > `OPN_TYPE_OP2REG)
    code=`OP_UNDEF;

//check opcode and optype if is allowable
case (data>>24)
   `EN_LOAD:
    begin   
        code=`OP_LOAD;
        n_type=data>>16&'hff;
        if(n_type!=`OPN_TYPE_OP1REG)        
            code=`OP_UNDEF;
        if(n_type==`OPN_TYPE_OP1REG)
            reg1=data>>8&'hff;
        if(n_type==`OPN_TYPE_OP2REG)
            reg2=data&'hff;
        if(n_type==`OPN_TYPE_OP2NUM)
            num=data&'hff;
        if(n_type==`OPN_TYPE_OP2MEM)
            mem=data&'hff;
    end
    `EN_STORE:      
    begin
    code=`OP_STORE;
    n_type=data>>16;
    if(n_type!=`OPN_TYPE_OP1MEM)
        code=`OP_UNDEF;

    if(n_type==`OPN_TYPE_OP2REG)
        reg2=data&'hff;
    if(n_type==`OPN_TYPE_OP1MEM)
        mem=data>>16&'hff;
    if(n_type==`OPN_TYPE_OP2NUM)
        num=data&'hff;
    end
    `EN_ADD:
    begin
    code=`OP_ADD;
    if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
        code=`OP_UNDEF;
    
    if(n_type==`OPN_TYPE_OP1REG)
        reg1=data>>8&'hff;
    if(n_type==`OPN_TYPE_OP2REG)
        reg2=data&'hff;
    if(n_type==`OPN_TYPE_OP2NUM)
        num=data&'hff;
    end
    `EN_SUB:
    begin
    code=`OP_SUB;
    if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)    
        code=`OP_UNDEF;

    if(n_type==`OPN_TYPE_OP1REG)
        reg1=data>>8'hff;
    if(n_type==`OPN_TYPE_OP2REG)
        reg2=data&'hff;
    if(n_type==`OPN_TYPE_OP2NUM)
        num=data&'hff;
    end
    `EN_MUL:
    begin
    code=`OP_MUL;
    if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
        code=`OP_UNDEF;
    
    if(n_type==`OPN_TYPE_OP1REG)
        reg1=data>>8'hff;
    if(n_type==`OPN_TYPE_OP2REG)
        reg2=data&'hff;
    if(n_type==`OPN_TYPE_OP2NUM)
        num=data&'hff;
    end
    `EN_DIV:
    begin
    code=`OP_DIV;
        if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
            code=`OP_UNDEF;

        if(n_type==`OPN_TYPE_OP1REG)
            reg1=data>>8'hff;
        if(n_type==`OPN_TYPE_OP2REG)
            reg2=data&'hff;
        if(n_type==`OPN_TYPE_OP2NUM)
            num=data&'hff;
    end
    `EN_INC:
    begin
    code=`OP_INC;
    if(n_type==`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    
    if(n_type==`OPN_TYPE_OP1REG)
        reg1=data>>8&'hf;
    if(n_type==`OPN_TYPE_OP1MEM)
        mem=data>>8&'hf;
    if(n_type==`OPN_TYPE_OP1NUM)
        num=data>>8&'hf;
    end
    `EN_DEC:
    begin
    code=`OP_DEC;
    if(n_type==`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    
    if(n_type==`OPN_TYPE_OP1REG)
        reg1=data>>8&'hf;
    if(n_type==`OPN_TYPE_OP1MEM)
        mem=data>>8&'hf;
    if(n_type==`OPN_TYPE_OP1NUM)
        num=data>>8&'hf;
    end
    `EN_AND:
    begin
    code=`OP_AND;
         if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
            code=`OP_UNDEF;
     end
    `EN_OR:
    begin
    code=`OP_OR;
      if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
            code=`OP_UNDEF;
    end
    `EN_XOR:
    begin
    code=`OP_XOR;
    if(n_type!=`OPN_TYPE_OP1REG||n_type==`OPN_TYPE_OP2MEM)
        code=`OP_UNDEF;;
    end
    `EN_NOT:
    begin
    code=`OP_NOT;
    if(n_type!=`OPN_TYPE_OP1REG)
        code=`OP_UNDEF;
    end
    `EN_JMP:
    begin
    code=`OP_JMP;
    if(n_type!=`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    end
    `EN_JA:
    begin
    code=`OP_JA;
    if(n_type!=`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    end
    `EN_JB:
    begin
    code=`OP_JB;
    if(n_type!=`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    end
    `EN_CMP:
    begin
    code=`OP_CMP;
    if(n_type!=`OPN_TYPE_ALLREG||n_type==`OPN_TYPE_ALLMEM)
        code=`OP_UNDEF;
    end
    `EN_HALT:
    begin
    code=`OP_HALT;
    end
    `EN_IN:
    begin
    code=`OP_INP;
    if(n_type!=`OPN_TYPE_OP1REG||n_type!=`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    end
    `EN_OUT:
    begin
    code=`OP_OUTP;
    if(n_type!=`OPN_TYPE_OP1REG||n_type!=`OPN_TYPE_OP1NUM)
        code=`OP_UNDEF;
    end
    default: 
    begin
    code=`OP_UNDEF;
    end             
endcase
end    

//read register 
always @(posedge clk) begin
core.reg_idx<=reg2;
core.reg_op<=`REG_READ;
#(`CLK_CYCLE);
en.reg2<=core.reg_out;

core.reg_idx<=reg1;
core.reg_op<=`REG_READ;
#(`CLK_CYCLE);
en.reg1<=core.reg_out;
end

endmodule
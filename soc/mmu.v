`include "header/ram.vh"
`include "header/register_group.vh"
`include "header/clock.vh"

//page descript filed
`define PAGE_DES_BASE_MASK 'hfff00000
`define PAGE_DES_P_MASK 'h00010000
`define PAGE_DES_D_MASK 'h00020000

`define PAGE_DES_SIZE 4

module mmu (
    input [0:31] addr,
    input clk,
    output reg [0:31] pyaddr
);

wire [0:31] page_dir_base='h0;
reg [0:31] pdir_off='h0;
reg [0:31] pt_off='h0;
reg [0:31] page_off='h0;

always @(posedge clk) begin
    core.reg_op <=`REG_READ;
    core.reg_idx <=`REG_CR2;
    #(`CLK_CYCLE);

    page_dir_base = core.reg_out;
    //figrue out page map idx info
    pdir_off = (addr >> 22)&'h3FF;
    pt_off = (addr>>12)&'h3FF;
    page_off = (addr)&'hFFF;

    //read pde
    core.ram_op <= `RAM_READ;
    core.ram_addr <= page_dir_base+pdir_off*`PAGE_DES_SIZE;
    #(`CLK_CYCLE)

    //check p bits
    if (core.ram_dataout&`PAGE_DES_P_MASK) 
    begin 
        //write pde dirty 
        core.ram_op <= `RAM_WRITE;
        core.ram_datain <= core.ram_dataout|`PAGE_DES_D_MASK;
        #(`CLK_CYCLE); 
            
        //read pte
        core.ram_op <= `RAM_READ;
        core.ram_addr <= core.ram_dataout&`PAGE_DES_BASE_MASK+pt_off*`PAGE_DES_SIZE;    
        #(`CLK_CYCLE);

        if(core.ramdata_out&`PAGE_DES_P_MASK)
        begin 
            //write pte dirty
            core.ram_op <= `RAM_WRITE;
            core.ram_datain = core.ram_dataout|`PAGE_DES_D_MASK;
            #(`CLK_CYCLE);

            //redirect to page offset
            pyaddr <= core.ramdata_out&`PAGE_DES_BASE_MASK+page_off;
        end
    end
end


endmodule
`include "header/clock.vh"

`timescale 1ns/1ns

module clock(output reg clk) ;
    initial clk = 1'b0 ;      //clk is initialized to "0"
    always    # (`CLK_CYCLE/2) clk = ~clk ;       //generating a real clock by reversing
 
    always begin
        #10;
        if ($time >= 1000) begin
            $finish ;
        end
    end

endmodule

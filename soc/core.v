`include "soc/register_group.v"
`include "soc/mmu.v"
`include "soc/rom.v"
`include "soc/ram.v"
`include "soc/alu.v"
`include "soc/fetch.v"
`include "soc/cu.v"

//cpu main core module
module core(input cpu_enable,input clk,input ext_int);

//reg group signal 
reg reg_rst='b1;
reg [0:7] reg_idx=0;
reg [0:31] reg_in=0;
reg reg_op='b0;
wire [0:31] reg_out=0;

//ram signal
reg [0:31] ram_addr=0;
reg ram_op=0;
reg [0:7] ram_datain=0;
wire [0:7]  ram_dataout;

//rom signal
wire [0:7] romdata_out;

//alu signal
reg [0:31] alu_data1;
reg [0:31] alu_data2;
reg [0:3] alu_op;
wire [0:31] alu_out1;
wire [0:31] alu_out2;

//mmu signal
reg [0:31] mu_vaddr;
wire [0:31] mu_paddr; 

//point current instruation 
reg [0:31] ip;   

//instructation cache buffer
reg [0:31] ic_buff; 

reg init_done='b0;  //cpu reset done

reg [0:3] cur_stu=0; //cpu current work unit 

wire [0:7] ins_op=0; 

//register module 
register_group reg_group(.clk (clk),.in(reg_in),.op (reg_op),.idx (reg_idx),.out (reg_out));
//ram modlue 
ram rammem(.clk (clk),.addr (ram_addr),.rw (ram_op),.data_in (ram_datain),.data_out (ramdata_out));
//rom module
rom rommem(.clk (clk),.addr (rom_addr),.data_out (romdata_out));
//cu module 
cu ctrlunit(.clk (clk), .en (init_done));
//alu module
alu alu_unit(.clk (clk),.data1(alu_op1),.data2(alu_op2),.op(alu_op),.res1 (alu_out1),.res2 (alu_out2));
//mmu module 
mmu mmu_unit(.clk (clk),.addr (mu_vaddr),.pyaddr(mu_pyaddr));
//fetch module 
fetch fetch_unit(.clk (clk));

//interrupt handle
always @(ext_int)
begin
     //external interrupt
end

//cpu reset 
always @(posedge clk)  
begin
     if(cpu_enable) //wait external enable signal from baseboard
     begin
          //reset register
          reg_rst<='b0;
          #(`CLK_CYCLE);
          reg_rst<='b1;
          #(`CLK_CYCLE);

          //set ip point to 0x200
          reg_idx <= `REG_IP;
          reg_in <= 'h200;
          reg_op <=`REG_WRITE;
          #(`CLK_CYCLE);
          //end init 
          init_done = 'b1;
     end
end

endmodule;
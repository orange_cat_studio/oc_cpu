//alu operator
`define ALU_ADD 'h0
`define ALU_SUB 'h1
`define ALU_MUL 'h2
`define ALU_DIV 'h3

`define ALU_AND 'h4
`define ALU_OR 'h5
`define ALU_XOR 'h6
`define ALU_NOT 'h7

`define ALU_SHL 'h8
`define ALU_SHR 'h9

`define ALU_INC 'h0A
`define ALU_DEC 'h0B
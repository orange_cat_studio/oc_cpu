//data register
`define REG_AR 'h0
`define REG_BR 'h1
`define REG_CR 'h2
`define REG_DR 'h3
`define REG_ER 'h4
`define REG_FR 'h5

//control register
`define REG_CR0 'h10
`define REG_CR1 'h11
`define REG_CR2 'h12 
`define REG_CR3 'h13 
`define REG_CR4 'h14 

//flags register
`define REG_FLAGS 'h07

//ip register
`define REG_IP 'h8

//idx register
`define REG_SI 'h9
`define REG_DI 'hA

//register op 
`define REG_READ  'b0
`define REG_WRITE 'b1

`define REG_MAX 255 